# document-db

- [document-db](#document-db)
  - [Introduction](#introduction)
  - [Database structure](#database-structure)
  - [Caveats](#caveats)
  - [Using](#using)
  - [Setting up](#setting-up)
  - [Examples](#examples)
  - [Testing](#testing)

## Introduction

This is a database that can store and read data in json and yaml formats. It tries to mimic spring data as close as possible, but there are some major differences, most obvius one is setting up repositories. The annotations are custom and not javax and most importantly this does not use Spring data commons
library.
The core package is just java implementation with gson and snakeyaml libraries and can be used in normal java applications. It is also possible to use database in different formats at the same time and rename base data and metadata file names.

## Database structure

Database has one base directory and subdirectories. Subdirectories are named after models and can be overridden. In each subdirectory, there is one data file and one metadata file. Data file consists of data inserted and metadata contains useful information about the model.

```
base dir
+-- model name
|   +-- data.json or yaml
|   +-- metadata.json or yaml
```



## Caveats

One of the biggest caveats are that arrays cannot store sub objects inside array item, only primitive types are supported like int, double, boolean, String, List. Another thing is that when adding item to data, the [ModelDbOperations](./src/main/java/ee/erik/documentdb/core/ModelDbArrayOperations.java) class implementation expects model to have id field. When there is a desire to use sub objects, split up models and use references.

## Using

There are two types of Daos, one is array dao that can store data in array format and another is object format which can
store data in single object format. Both have the same limitation as mentioned above. Object format dao doesn't need id field.

## Setting up

Clone this repository or add it as submodule to your spring boot project. Then in root project settings.gradle add include 'document-db' and in build.gradle add implementation project('document-db').

Then in application or somewhere in configuration class add [@EnableDocumentDb](./src/main/java/ee/erik/documentdb/config/EnableDocumentDb.java) annotation.

Then in application properties there are 4 config properties for this library.

- ee.erik.documentdb.archive-name=your database name
- ee.erik.documentdb.base-file-type=json format its json or yaml
- ee.erik.documentdb.lifetime=transient database lifetime, currently only has Persist and Transient mode.

What's left to do is implement repositories with models in your project.

## Examples

Examples are located at [here](./src/test/java/ee/erik/documentdb/integration) and check the implementation of testrepositories: [This file](./src/test/java/ee/erik/documentdb/integration/list/DatabaseListRepoTest.java) contains tests of reading, writing, updating and deleting data using setup folders implementations.

## Testing

Run test task in IntelliJ Idea or from command line using command:

`./gradlew.bat test -i` on windows or `./gradlew test -i` on linux.
