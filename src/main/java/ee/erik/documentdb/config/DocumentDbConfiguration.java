package ee.erik.documentdb.config;

import ee.erik.documentdb.properties.DocumentDbProperties;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.LinkedList;
import java.util.List;

@Configuration
@EnableConfigurationProperties(DocumentDbProperties.class)
public class DocumentDbConfiguration {

    private final DocumentDbProperties documentDbProperties;

    public DocumentDbConfiguration(DocumentDbProperties documentDbProperties) {
        this.documentDbProperties = documentDbProperties;
    }
}
