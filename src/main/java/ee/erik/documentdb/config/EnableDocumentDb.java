package ee.erik.documentdb.config;

import ee.erik.documentdb.dao.ListListGenericDaoImpl;
import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation just loads database configuration properties to application
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Import({DocumentDbConfiguration.class, ListListGenericDaoImpl.class})
public @interface EnableDocumentDb {
}
