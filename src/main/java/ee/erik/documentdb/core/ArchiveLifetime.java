package ee.erik.documentdb.core;

/**
 * This property defines how database file should be kept on storage
 */
public enum ArchiveLifetime {
    /**
     * Transient mode will briefly store files on disk,
     * data will be deleted once application shuts down or
     * when purge is manually called
     */
    Transient,
    /**
     * Persist mode will keep files permanently on disk
     */
    Persist
}
