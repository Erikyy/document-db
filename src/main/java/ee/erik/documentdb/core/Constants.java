package ee.erik.documentdb.core;

public class Constants {

    public static String METADATA_ID_DEFINITION = "id_def";
    public static String METADATA_ID_GENERATION_TYPE = "id_gen_type";
    public static String METADATA_ID_AUTOINCREMENT = "autoincrement";
    public static String METADATA_ID_REFERENCE = "reference_type";
    public static String METADATA_ID_REFERENCE_MANY = "reference_many";
    public static String METADATA_ID_REFERENCE_ONE = "reference_one";

    public static String METADATA_FIELD_NAME = "field_name";
    public static String METADATA_FIELD_TYPE = "field_type";

    /**
     * This is for when a type is in a container like type T in List<T>
     */
    public static String METADATA_FIELD_SUB_TYPE = "field_sub_type";
    public static String METADATA_FIELD_OLD_NAME = "old_name";
    //allows writes to database file as array of objects
    public static String METADATA_OBJECT_WRITE_CONFIG_ARRAY = "array";
    //allows writes to database file as single object
    public static String METADATA_OBJECT_WRITE_CONFIG_OBJECT = "object";

}
