package ee.erik.documentdb.core;

public enum FileType {
    Json,
    Yaml
}
