package ee.erik.documentdb.core;

import ee.erik.documentdb.core.annotations.GenerationType;
import ee.erik.documentdb.core.metadata.AnnotationProcessor;
import org.apache.commons.lang3.tuple.Triple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ModelDbArrayOperations<T, ID> {

    private final ModelDbEngine<T> databaseEngine;

    private static final Logger logger = LoggerFactory.getLogger(ModelDbArrayOperations.class);

    /**
     * Generates object metadata and data
     * @param object
     */
    public ModelDbArrayOperations(Class<T> object, ModelDbConfiguration modelDbConfiguration) {
        this.databaseEngine = new ModelDbEngine<>(object, modelDbConfiguration);
    }

    /**
     * Inserts new object to data file
     * Returns inserted object when it has successfully saved.
     * If some error occurs while reading or writing data, it will return null
     * @param object
     * @return
     */
    public T insert(T object) {
        //first validate this object then check id generation type
        //then get data from file and then add object to that array
        //and finally write the new array back to file
        this.databaseEngine.validateObject(object);

        if (this.databaseEngine.getMetadata().getDataSaveConfig().equals(Constants.METADATA_OBJECT_WRITE_CONFIG_ARRAY)) {
            //array inserts here
            List<T> data;
            try {
                data = this.databaseEngine.readDataAsArray();
            } catch (Exception e) {
                logger.error("Failed to read data from " + object.getClass().getSimpleName() + " data");
                return null;
            }

            Triple<GenerationType, String, String> genData = this.databaseEngine.getGenerationTypeIdTypeAndIdField(object);
            if (data.isEmpty()) {
                if (genData.getRight().equals(Long.class.getSimpleName())) {
                    AnnotationProcessor.setFieldValue(genData.getMiddle(), 0L, object);
                } else if (genData.getRight().equals(Integer.class.getSimpleName())) {
                    AnnotationProcessor.setFieldValue(genData.getMiddle(), 0, object);
                }
            } else {
                T last = data.get(data.size() - 1);
                Triple<GenerationType, String, String> lastGenData = this.databaseEngine.getGenerationTypeIdTypeAndIdField(last);
                if (genData.getLeft() == GenerationType.AutoIncrement) {
                    //get last id value and increment by one
                    //types have to be same
                    if (genData.getRight().equals(lastGenData.getRight())) {
                        if (genData.getRight().equals(Long.class.getSimpleName())) {
                            Long lastIdVal = (Long) AnnotationProcessor.getObjectValues(last).get(lastGenData.getMiddle());
                            lastIdVal += 1L;
                            AnnotationProcessor.setFieldValue(genData.getMiddle(), lastIdVal, object);
                        } else if (genData.getRight().equals(Integer.class.getSimpleName())) {
                            Integer lastIdVal = (Integer) AnnotationProcessor.getObjectValues(last).get(lastGenData.getMiddle());
                            lastIdVal += 1;
                            AnnotationProcessor.setFieldValue(genData.getMiddle(), lastIdVal, object);
                        }
                    }
                }
            }

            data.add(object);
            try {
                this.databaseEngine.writeDataAsArray(data);
            } catch (Exception e) {
                logger.error("Failed to write data to database");
                return null;
            }
        } else if (this.databaseEngine.getMetadata().getDataSaveConfig().equals(Constants.METADATA_OBJECT_WRITE_CONFIG_OBJECT)){
            try {
                this.databaseEngine.writeDataAsObject(object);
            } catch (Exception e) {
                logger.error("Failed to write data to database");
                return null;
            }
        }


        return object;
    }

    /**
     * Updated item in database,
     * should it fail it will log error and return null value
     *
     * @param object
     * @return
     */
    public T update(T object) {
        this.databaseEngine.validateObject(object);

        if (this.databaseEngine.getMetadata().getDataSaveConfig().equals(Constants.METADATA_OBJECT_WRITE_CONFIG_ARRAY)) {
            List<T> data;

            try {
                data = this.databaseEngine.readDataAsArray();
            } catch (Exception e) {
                logger.error("Failed to read data from " + object.getClass().getSimpleName() + " data");
                return null;
            }

            Triple<GenerationType, String, String> objectGenData = this.databaseEngine.getGenerationTypeIdTypeAndIdField(object);

            //take the object from array and compare the ids
            Optional<T> first = data.stream().filter(p -> {
                Triple<GenerationType, String, String> arrayItemGenData = this.databaseEngine.getGenerationTypeIdTypeAndIdField(p);
                ID firstId = (ID) AnnotationProcessor.getObjectValues(object).get(arrayItemGenData.getMiddle());
                ID newId = (ID) AnnotationProcessor.getObjectValues(p).get(objectGenData.getMiddle());
                return firstId == newId;
            }).findFirst();

            if (first.isPresent()) {
                //insert new object to array at older version of that objects index
                int objIndex = data.indexOf(first.get());
                data.set(objIndex, object);
            } else {
                logger.error("Object not found in " + object.getClass().getSimpleName() + " data");
                return null;
            }
            try {
                this.databaseEngine.writeDataAsArray(data);
            } catch (Exception e) {
                return null;
            }
        } else if (this.databaseEngine.getMetadata().getDataSaveConfig().equals(Constants.METADATA_OBJECT_WRITE_CONFIG_OBJECT)) {
            try {
                this.databaseEngine.writeDataAsObject(object);
            } catch (Exception e) {
                logger.error("Failed to write data to database");
                return null;
            }
        }

        return object;
    }


    public void delete(T object) {
        try {
            List<T> data = this.databaseEngine.readDataAsArray();
            data.remove(object);
            this.databaseEngine.writeDataAsArray(data);
        } catch (Exception e) {
            logger.error("Failed to delete object " + object.getClass().getSimpleName() + " data");
        }

    }

    /**
     * Find object by id
     * Returns empty when some error has occurred while reading data
     * @param id
     * @return
     */
    public Optional<T> findById(ID id) {
        try {
            List<T> data = this.databaseEngine.readDataAsArray();

            return data.stream().filter(p -> {
                Triple<GenerationType, String, String> genData = this.databaseEngine.getGenerationTypeIdTypeAndIdField(p);
                ID pId = (ID) AnnotationProcessor.getObjectValues(p).get(genData.getMiddle());
                return pId == id;
            }).collect(Collectors.toSet()).stream().findFirst();
        } catch (Exception e) {
            logger.error("Failed to read data");
            return Optional.empty();
        }
    }


    public T getObject() {
        try {
            return this.databaseEngine.readDataAsObject();
        } catch (Exception e) {
            logger.error("Failed to read data");
            return null;
        }
    }

    /**
     * Get all data from model data file
     * returns null if something went wrong with data reading
     * @return
     */
    public List<T> findAll() {
        try {
            return this.databaseEngine.readDataAsArray();
        } catch (Exception e) {
            logger.error("Failed to read data");
            return null;
        }
    }

    /**
     * !!! Warning this method will delete all data in model name directory
     * Usually when archive lifetime is set to Transient or InMemory
     * it will delete folders with no warnings, Persist mode will need override-persist-delete set to true
     * in order to delete data in Persist mode.
     */
    public void destroy() {
        this.databaseEngine.destroy();
    }
}
