package ee.erik.documentdb.core;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ModelDbConfiguration {
    private FileType fileType;
    private ArchiveLifetime lifetime;
    private String baseArchiveName;
    private boolean overridePersistDelete;
}
