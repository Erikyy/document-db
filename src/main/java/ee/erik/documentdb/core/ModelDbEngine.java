package ee.erik.documentdb.core;

import com.google.gson.internal.LinkedTreeMap;
import ee.erik.documentdb.core.annotations.GenerationType;
import ee.erik.documentdb.core.file.FileUtils;
import ee.erik.documentdb.core.metadata.*;
import ee.erik.documentdb.core.parser.AbstractParser;
import ee.erik.documentdb.core.parser.JsonParser;
import ee.erik.documentdb.core.parser.YamlParser;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ModelDbEngine<T> {

    private ModelDbConfiguration configuration;

    private final AnnotationProcessor annotationProcessor;

    private final FileUtils fileUtils;
    private DatabaseObjectMetadata metadata;

    private final AbstractParser<T> parser;

    private final String metadataFileName;
    private final String dataFileName;

    private final Class<T> model;

    private static final Logger logger = LoggerFactory.getLogger(ModelDbEngine.class);

    public ModelDbEngine(Class<T> model, ModelDbConfiguration configuration) {
        this.configuration = configuration;
        this.annotationProcessor = new AnnotationProcessor(model);
        this.model = model;
        //check if model has overridden data file name or metadata file name or file type

        String dataName = this.annotationProcessor.getModelDataFileName("data");
        String metadataName = this.annotationProcessor.getModelMetadataFileName("metadata");

        FileType fileType = this.annotationProcessor.getModelFileType(configuration.getFileType());

        this.dataFileName = switch (fileType) {
            case Json -> dataName + ".json";
            case Yaml -> dataName + ".yaml";
        };
        this.metadataFileName = switch (fileType) {
            case Json -> metadataName + ".json";
            case Yaml -> metadataName + ".yaml";
        };
        this.fileUtils = new FileUtils(configuration.getBaseArchiveName())
                .createSubDirectory(annotationProcessor.getModelName())
                .createFile(this.dataFileName)
                .createFile(this.metadataFileName);

        try {
            this.metadata = MetadataUtils.generateMetadata(fileUtils.getFile(this.metadataFileName), annotationProcessor, fileType);
        } catch (Exception e) {
            logger.error("Failed to generate metadata. Try restarting application.");
        }

        this.parser = switch (fileType) {
            case Json -> new JsonParser<>(this.annotationProcessor, this.metadata);
            case Yaml -> new YamlParser<>(this.annotationProcessor, this.metadata);
        };

        //perform migrations here or generate new data
        this.doChangesInDataFile();
    }

    /**
     *
     * @param object
     * @return generation type, id field name and id type from metadata
     */
    public Triple<GenerationType, String, String> getGenerationTypeIdTypeAndIdField(T object) {
        String idType = "";
        String idField = " ";
        GenerationType generationType = GenerationType.None;

        for (Map.Entry<String, DatabasePropertyData> entry : this.metadata.getPropertyDataMap().entrySet()) {
            if (entry.getValue().getExtraData().containsKey(Constants.METADATA_ID_DEFINITION)) {
                idField = entry.getValue().getExtraData().get(Constants.METADATA_FIELD_NAME);
                idType = entry.getValue().getExtraData().get(Constants.METADATA_FIELD_TYPE);
                if (entry.getValue().getExtraData().containsKey(Constants.METADATA_ID_GENERATION_TYPE)) {
                    if (entry.getValue().getExtraData().get(Constants.METADATA_ID_GENERATION_TYPE).equals(Constants.METADATA_ID_AUTOINCREMENT)) {
                        generationType = GenerationType.AutoIncrement;
                    }
                }
            }

        }
        AnnotationProcessor.getObjectValues(object);
        return new ImmutableTriple<>(generationType, idField, idType);
    }

    private void doChangesInDataFile() {
        logger.info("Checking changes in metadata and updating database");
        List<Map.Entry<String, DatabasePropertyData>> objectProperties = this.metadata.getPropertyDataMap().entrySet().stream().toList();
        if (this.metadata.getDataSaveConfig().equals(Constants.METADATA_OBJECT_WRITE_CONFIG_ARRAY)) {
            List<LinkedTreeMap<String, Object>> dbEntries;
            try {
                dbEntries = this.fromDataToListLinkedHashMap();
            } catch (Exception e) {
                logger.error("Failed to get data from database");
                return;
            }

            if (dbEntries == null) {
                //write new data
                try {
                    this.writeDataAsArray(new ArrayList<>());
                } catch (Exception e) {
                    logger.error("Failed to write new data!");
                }
            } else {
                if (!dbEntries.isEmpty()) {
                    //insert values to this list
                    List<LinkedTreeMap<String, Object>> newDbEntries = new ArrayList<>();
                    //iterate through all data entries

                    for (LinkedTreeMap<String, Object> dbEntry : dbEntries) {
                        LinkedTreeMap<String, Object> newDbEntry = this.createNewEntry(dbEntry, objectProperties);
                        newDbEntries.add(newDbEntry);
                    }
                    try {
                        this.writeRawToData(newDbEntries);
                    } catch (Exception e) {
                        logger.error("Failed to write new entries to database");
                    }
                } else {
                    logger.info("Database entries have not changed.");
                }
            }
        } else if (this.metadata.getDataSaveConfig().equals(Constants.METADATA_OBJECT_WRITE_CONFIG_OBJECT)) {
            //save here as object
            LinkedTreeMap<String, Object> dbEntry;
            try {
                dbEntry = this.parser.readDataAsRawObject(this.fileUtils.getFile(this.dataFileName));
            } catch (Exception e) {
                logger.error("Failed to get data from database");
                return;
            }
            //if data is null then do nothing, it is an object not array
            if (dbEntry != null) {
                LinkedTreeMap<String, Object> newDbEntry = this.createNewEntry(dbEntry, objectProperties);
                try {
                    this.parser.writeRawAsDataObject(newDbEntry, this.fileUtils.getFile(this.dataFileName));
                } catch (Exception e) {
                    logger.error("Failed to write to database");
                }
            }
        }
    }

    private LinkedTreeMap<String, Object> createNewEntry(LinkedTreeMap<String, Object> dbEntry, List<Map.Entry<String, DatabasePropertyData>> objectProperties) {
        LinkedTreeMap<String, Object> newEntry = new LinkedTreeMap<>();
        for (Map.Entry<String, DatabasePropertyData> property : objectProperties) {
            //get either current or old property to get value from old database entry
            String propertyName;
            if (property.getValue().getExtraData().get(Constants.METADATA_FIELD_OLD_NAME) == null) {
                propertyName = property.getKey();
            } else {
                propertyName = property.getValue().getExtraData().get(Constants.METADATA_FIELD_OLD_NAME);
            }
            if (dbEntry.containsKey(propertyName)) {
                Object val = dbEntry.get(propertyName);
                //here we just replace the name of property and assign the same value to it
                newEntry.put(property.getKey(), val);
            } else {
                //here the property does not exist so create new one
                this.assignValuesToProperties(newEntry, property);
            }
        }
        return newEntry;
    }

    private void assignValuesToProperties(LinkedTreeMap<String, Object> newDbEntry, Map.Entry<String, DatabasePropertyData> property) {
        String propertyType = property.getValue().getExtraData().get(Constants.METADATA_FIELD_TYPE);
        if (property.getValue().isNullable()) {
            switch (propertyType) {
                case "Long", "String", "Integer", "Boolean" -> {
                    newDbEntry.put(property.getKey(), null);
                }
                case "long" -> {
                    newDbEntry.put(property.getKey(), 0L);
                }
                case "int" -> {
                    newDbEntry.put(property.getKey(), 0);
                }
                case "boolean" -> {
                    newDbEntry.put(property.getKey(), false);
                }
                case "List" -> {
                    //in case of list just put empty array
                    newDbEntry.put(property.getKey(), new ArrayList<>());
                }
            }
        } else {
            //check types and put empty default value to it
            switch (propertyType) {
                case "Long", "long" -> {
                    newDbEntry.put(property.getKey(), 0L);
                }
                case "String" -> {
                    newDbEntry.put(property.getKey(), "");
                }
                case "int", "Integer" -> {
                    newDbEntry.put(property.getKey(), 0);
                }
                case "boolean", "Boolean" -> {
                    newDbEntry.put(property.getKey(), false);
                }
                case "List" -> {
                    //in case of list just put empty array
                    newDbEntry.put(property.getKey(), new ArrayList<>());
                }
            }
        }
    }

    public void writeDataAsArray(List<T> object) throws Exception {
        //get array from file and append this object to that array
        this.parser.writeArrayAsData(object, this.fileUtils.getFile(this.dataFileName));
    }

    public List<T> readDataAsArray() throws Exception {
        return this.parser.readDataAsList(this.fileUtils.getFile(this.dataFileName));
    }

    /**
     * Write object to data with adapter that uses metadata
     * @param object
     * @throws Exception
     */
    public void writeDataAsObjectWithAdapter(T object) throws Exception {
        this.parser.writeObjectAsDataWithAdapter(object, this.fileUtils.getFile(this.dataFileName));
    }

    /**
     * Read object data with adapter that uses metadata
     * @return
     * @throws Exception
     */
    public T readDataAsObjectWithAdapter() throws Exception {
        return this.parser.readDataAsObjectWithAdapter(this.fileUtils.getFile(this.dataFileName), this.model);
    }

    /**
     * These functions below just write objects to database
     * They don't use custom adapters, only for simple stuff that don't need annotations
     * @param object
     * @throws Exception
     */
    public void writeDataAsObject(T object) throws Exception {
        this.parser.writeObjectAsData(object, this.fileUtils.getFile(this.dataFileName));
    }

    /**
     * These functions below just write objects to database
     * They don't use custom adapters, only for simple stuff that don't need annotations
     * @return
     * @throws Exception
     */
    public T readDataAsObject() throws Exception {
        return this.parser.readDataAsObject(this.fileUtils.getFile(this.dataFileName), this.annotationProcessor.getObject());
    }

    /**
     * This is only used for updating data file when application starts up
     * @return file data as raw map
     */
    private List<LinkedTreeMap<String, Object>> fromDataToListLinkedHashMap() throws Exception {
        return this.parser.readDataAsRawArray(this.fileUtils.getFile(this.dataFileName));
    }

    /**
     * Simple object validation with annotations,
     * checks if fields are nullable,
     * if fields are not nullable and value is null it will throw NullPointerException
     * @param object
     */
    public void validateObject(T object) {
        Set<FieldData> fieldDataSet = this.annotationProcessor.getModelFieldsData();
        Map<String, Object> fieldValues = AnnotationProcessor.getObjectValues(object);

        //nullable is really the only thing validated
        for (FieldData fieldData : fieldDataSet) {
            if (!fieldData.isNullable()) {
                //if field has generation type annotation then null check should be skipped for that field.
                if (!fieldData.getExtraData().containsKey(Constants.METADATA_ID_GENERATION_TYPE)) {
                    Object value = fieldValues.get(fieldData.getExtraData().get(Constants.METADATA_FIELD_NAME));
                    if (value == null) {
                        throw new NullPointerException(fieldData.getName() + " : " + fieldData.getExtraData().get(Constants.METADATA_FIELD_NAME) + " : is set to null but is non-nullable field!");
                    }
                }

            }
        }
    }

    public DatabaseObjectMetadata getMetadata() {
        return metadata;
    }

    /**
     * This is only used for updating data file when application starts up
     * @return file data as raw map
     */
    private void writeRawToData(List<LinkedTreeMap<String, Object>> map) throws Exception {
        this.parser.writeRawAsDataArray(map, this.fileUtils.getFile(this.dataFileName));
    }

    /**
     * This method is dangerous, only Transient mode is normally allowed unless override flag is set to true.
     * It will only show warning in console if there is an attempt to delete data in Persist mode.
     */
    public void destroy() {
        logger.info("Attempting to destroy database in " + this.configuration.getLifetime() + " mode");
        switch (this.configuration.getLifetime()) {
            case Transient -> {
                try {
                    this.fileUtils.delete();
                    //if this reaches here it is safe to assume the files are deleted
                    logger.info("Database successfully destroyed");
                } catch (Exception e) {
                    logger.error("Failed to destroy database: " + e.getMessage());
                }
            }
            case Persist -> {
                if (this.configuration.isOverridePersistDelete()) {
                    try {
                        this.fileUtils.delete();
                        logger.info("Database successfully destroyed");
                    } catch (Exception e) {
                        logger.error("Failed to destroy database: " + e.getMessage());
                    }
                } else {
                    logger.warn("Attempted to delete database data in Persist mode however overridePersistDelete flag is set to false");
                }
            }
        }
    }
}
