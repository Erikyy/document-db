package ee.erik.documentdb.core;

import ee.erik.documentdb.core.annotations.GenerationType;
import ee.erik.documentdb.core.metadata.AnnotationProcessor;
import org.apache.commons.lang3.tuple.Triple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ModelDbObjectOperations<T> {

    private final ModelDbEngine<T> databaseEngine;

    private static final Logger logger = LoggerFactory.getLogger(ModelDbObjectOperations.class);

    /**
     * Generates object metadata and data
     * @param object
     */
    public ModelDbObjectOperations(Class<T> object, ModelDbConfiguration modelDbConfiguration) {
        this.databaseEngine = new ModelDbEngine<>(object, modelDbConfiguration);
    }

    /**
     * Saves or updates object in data file
     * Returns saved object when it has successfully saved.
     * If some error occurs while reading or writing data, it will return null
     * @param object
     * @return
     */
    public T save(T object) {
        //first validate this object then check id generation type
        //then get data from file and then add object to that array
        //and finally write the new array back to file
        this.databaseEngine.validateObject(object);

        try {
            this.databaseEngine.writeDataAsObjectWithAdapter(object);
        } catch (Exception e) {
            logger.error("Failed to write data to database");
            return null;
        }
        return object;
    }

    /**
     * This will remove the entire object but not the data file, other way to remove all data is using destroy method
     *
     */
    public void delete() {
        //write empty object
        try {
            this.databaseEngine.writeDataAsObject(null);
        } catch (Exception e) {
            logger.error("Failed to delete data");
        }
    }

    /**
     *
     * @return Returns null when there were some issues reading the data
     */
    public T getObject() {
        try {
            return this.databaseEngine.readDataAsObjectWithAdapter();
        } catch (Exception e) {
            logger.error("Failed to read data. Object is possibly null. Message: " + e.getMessage());
            return null;
        }
    }

    /**
     * !!! Warning this method will delete all data in model name directory
     * Usually when archive lifetime is set to Transient or InMemory
     * it will delete folders with no warnings, Persist mode will need override-persist-delete set to true
     * in order to delete data in Persist mode.
     */
    public void destroy() {
        this.databaseEngine.destroy();
    }
}
