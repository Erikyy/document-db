package ee.erik.documentdb.core.file;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class FileUtils {

    private final File baseDir;
    private File subDir;

    private final Map<String, File> files;

    public FileUtils(String name) {
        File file = new File(name);
        if (!file.exists()) {
            if (!file.mkdir()) {
                System.out.println("Directory: " + file.getAbsolutePath() + " already exists!");
            }
        }

        this.baseDir = file;
        this.subDir = null;
        this.files = new HashMap<>();
    }

    public FileUtils createSubDirectory(String name) {

        File file = new File(this.baseDir, name);
        if (!file.exists()) {
            if (!file.mkdir()) {
                System.out.println("Directory: " + this.baseDir.getAbsolutePath() + "/" + name + " already exists!");
            }
        }
        this.subDir = file;
        return this;
    }

    public FileUtils createFile(String name) {
        File file = new File(this.subDir, name);

        try {
            if (!file.exists()) {
                if (!file.createNewFile()) {
                    System.out.println("File in " + this.baseDir.getAbsolutePath() + "/" + this.subDir.getPath() + "/" + name + " already exist");
                }
            }
            this.files.put(name, file);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return this;
    }

    public File getFile(String name) {
        return this.files.get(name);
    }

    /**
     * Deletes subdirectory and its contents from base directory
     */
    public void delete() throws Exception {
        this.files.clear();
        Files.walk(this.baseDir.toPath())
                    .sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .forEach(File::delete);

    }
}
