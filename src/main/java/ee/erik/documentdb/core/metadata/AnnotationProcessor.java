package ee.erik.documentdb.core.metadata;

import ee.erik.documentdb.core.Constants;
import ee.erik.documentdb.core.FileType;
import ee.erik.documentdb.core.annotations.*;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.*;

public class AnnotationProcessor {

    private final Class<?> object;

    /**
     * Parses object annotations and id field type, checks if they're present
     * if not then return field or class name
     */
    public AnnotationProcessor(Class<?> object) {
        this.object = object;
    }

    public String getModelName() {
        if (this.object.isAnnotationPresent(DatabaseObject.class)) {
            DatabaseObject databaseObject = this.object.getAnnotation(DatabaseObject.class);
            if (databaseObject.name().equals("")) {
                return this.object.getSimpleName();
            } else {
                return databaseObject.name();
            }
        } else if (this.object.isAnnotationPresent(DatabaseObjectAsSingleton.class)) {
            DatabaseObjectAsSingleton databaseObjectAsSingleton = this.object.getAnnotation(DatabaseObjectAsSingleton.class);
            if (databaseObjectAsSingleton.name().equals("")) {
                return this.object.getSimpleName();
            } else {
                return databaseObjectAsSingleton.name();
            }
        } else {
            return this.object.getSimpleName();
        }
    }

    /**
     * Get database write type configuration
     * @return
     */
    public String getModelDataWriteConfiguration() {
        if (this.object.isAnnotationPresent(DatabaseObject.class)) {
            return Constants.METADATA_OBJECT_WRITE_CONFIG_ARRAY;
        } else if (this.object.isAnnotationPresent(DatabaseObjectAsSingleton.class)) {
            return Constants.METADATA_OBJECT_WRITE_CONFIG_OBJECT;
        } else {
            //default configuration is array
            return Constants.METADATA_OBJECT_WRITE_CONFIG_ARRAY;
        }
    }

    public FileType getModelFileType(FileType defaultFileType) {
        if (this.object.isAnnotationPresent(OverrideFileType.class)) {
            OverrideFileType databaseObject = this.object.getAnnotation(OverrideFileType.class);
            return databaseObject.fileType();
        } else {
            return defaultFileType;
        }
    }

    public String getModelDataFileName(String defaultName) {
        if (this.object.isAnnotationPresent(OverrideDataFileName.class)) {
            OverrideDataFileName overrideDataFileName = this.object.getAnnotation(OverrideDataFileName.class);
            if (overrideDataFileName.name().equals("")) {
                return defaultName;
            } else {
                return overrideDataFileName.name();
            }
        } else {
            return defaultName;
        }
    }

    public String getModelMetadataFileName(String defaultName) {
        if (this.object.isAnnotationPresent(OverrideMetadataFileName.class)) {
            OverrideMetadataFileName overrideMetadataFileName = this.object.getAnnotation(OverrideMetadataFileName.class);
            if (overrideMetadataFileName.name().equals("")) {
                return defaultName;
            } else {
                return overrideMetadataFileName.name();
            }
        } else {
            return defaultName;
        }
    }

    public Set<FieldData> getModelFieldsData() {
        Set<FieldData> fieldNames = new LinkedHashSet<>();
        for (Field f : Arrays.stream(this.object.getDeclaredFields()).toList()) {
            FieldData fieldData = new FieldData();
            fieldData.setName(this.parseFieldName(f));
            fieldData.setNullable(this.isNullable(f));
            Map<String, String> extraData = new HashMap<>();

            this.parseId(f, extraData);
            extraData.put(Constants.METADATA_FIELD_NAME, f.getName());
            extraData.put(Constants.METADATA_FIELD_TYPE, f.getType().getSimpleName());
            this.putSubFieldType(f, extraData);
            fieldData.setExtraData(extraData);
            fieldNames.add(fieldData);
        }
        return fieldNames;
    }

    public static <T> Map<String, Object> getObjectValues(T object) {
        Map<String, Object> values = new HashMap<>();

        for (Field f : object.getClass().getDeclaredFields()) {
            try {
                f.setAccessible(true);
                Object val = f.get(object);
                values.put(f.getName(), val);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return values;
    }

    private String parseFieldName(Field f) {
        if (f.isAnnotationPresent(ObjectProperty.class)) {
            ObjectProperty property = f.getAnnotation(ObjectProperty.class);
            if (property.name().equals("")) {
                return f.getName();
            } else {
                return property.name();
            }
        } else {
            return f.getName();
        }
    }

    private boolean isNullable(Field f) {
        if (f.isAnnotationPresent(Id.class)) {
            //id must have some value
            return false;
        } else {
            return !f.isAnnotationPresent(NotNull.class);
        }
    }

    private void parseId(Field f, Map<String, String> extraData) {
        if (f.isAnnotationPresent(Id.class)) {
            //the id definition key is important, it's value is not
            extraData.put(Constants.METADATA_ID_DEFINITION, "id_field");
            if (f.isAnnotationPresent(GeneratedId.class)) {
                GeneratedId generatedId = f.getAnnotation(GeneratedId.class);
                switch (generatedId.generationType()) {
                    case AutoIncrement -> {
                        extraData.put(Constants.METADATA_ID_GENERATION_TYPE, Constants.METADATA_ID_AUTOINCREMENT);
                    }
                }
            }
        }
    }


    public <T> T newInstanceOfT() {
        try {
            Constructor<T> constructor = (Constructor<T>) this.object.getDeclaredConstructor();
            return constructor.newInstance();
        } catch (Exception e) {
            throw new RuntimeException("No constructor found");
        }

    }

    public static <T> void setFieldValue(String fieldName, Object value, T instance) {
        try {
            Field field = instance.getClass().getDeclaredField(fieldName);
            field.setAccessible(true);
            field.set(instance, value);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void putSubFieldType(Field f, Map<String, String> extraData) {
        //get list type
        if (f.getType().getSimpleName().equals("List")
        || f.getType().getSimpleName().equals("ArrayList")
        ) {
            ParameterizedType parameterizedType = (ParameterizedType) f.getGenericType();
            Class<?> actualType = (Class<?>) parameterizedType.getActualTypeArguments()[0];
            extraData.put(Constants.METADATA_FIELD_SUB_TYPE, actualType.getSimpleName());
        }
    }

    public <T> Class<T> getObject() {
        return (Class<T>) object;
    }
}
