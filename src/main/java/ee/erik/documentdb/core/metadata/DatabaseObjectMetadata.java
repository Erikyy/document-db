package ee.erik.documentdb.core.metadata;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

/**
 * json format
 * {
 *     mappedProperties: {
 *         "property_name": {
 *             "default_value": "some value"
 *         },
 *         "property_name_2": {
 *             "default_value": null
 *         },
 *         "property_name_reference_to_other": {
 *             "nullable": true,
 *             "extraData": {
 *
 *             }
 *         },
 *     }
 * }
 *
 *  Metadata is used for value checking whether some value can be
 *  nullable or is unique etc.
 */
@Data
@NoArgsConstructor
public class DatabaseObjectMetadata {
    /**
     * This field is basically for checking if model is annotated as DatabaseObject or DatabaseObjectAsSingle.
     */
    private String dataSaveConfig;
    private Map<String, DatabasePropertyData> propertyDataMap;
}
