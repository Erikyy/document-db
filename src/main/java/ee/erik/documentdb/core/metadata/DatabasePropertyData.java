package ee.erik.documentdb.core.metadata;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DatabasePropertyData {
    private boolean nullable;
    private Map<String, String> extraData;
}
