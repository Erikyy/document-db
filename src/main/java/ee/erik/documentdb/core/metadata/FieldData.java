package ee.erik.documentdb.core.metadata;

import lombok.Data;

import java.util.Map;

@Data
public class FieldData {
    private String name;
    private boolean nullable;
    private Map<String, String> extraData;

}
