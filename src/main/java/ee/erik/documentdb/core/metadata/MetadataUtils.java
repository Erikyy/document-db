package ee.erik.documentdb.core.metadata;

import ee.erik.documentdb.core.Constants;
import ee.erik.documentdb.core.FileType;
import ee.erik.documentdb.core.parser.AbstractParser;
import ee.erik.documentdb.core.parser.JsonParser;
import ee.erik.documentdb.core.parser.YamlParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

public class MetadataUtils {

    private static final Logger logger = LoggerFactory.getLogger(MetadataUtils.class);

    /**
     * Creates new/updates existing/uses existing metadata.
     *
     * @param file mappings file
     * @param annotationProcessor annotation processor instance
     * @param fileType File type json or yaml
     * @return Metadata and changed, changed returns true when metadata from mappings file
     *  is out of date compared to database object otherwise it will return false
     */
    public static DatabaseObjectMetadata generateMetadata(File file, AnnotationProcessor annotationProcessor, FileType fileType) throws Exception {
        logger.info("Generating metadata");
        AbstractParser<DatabaseObjectMetadata> parser = switch (fileType) {
            case Json -> new JsonParser<>();
            case Yaml -> new YamlParser<>();
        };

        DatabaseObjectMetadata metadata = parser.readDataAsObject(file, DatabaseObjectMetadata.class);
        if (metadata == null) {
            logger.info("No metadata found, generating new");
            metadata = createNewMetadata(annotationProcessor);
            parser.writeObjectAsData(metadata, file);
            return metadata;
        } else {
            DatabaseObjectMetadata newMetadata = createFromExistingMetadata(metadata, annotationProcessor);
            if (metadata.equals(newMetadata)) {
                logger.info("No changes in metadata detected");
                return metadata;
            } else {
                logger.info("Changes in metadata detected. Writing new metadata to file");
                parser.writeObjectAsData(newMetadata, file);
                return newMetadata;
            }
        }
    }

    private static DatabaseObjectMetadata createNewMetadata(AnnotationProcessor annotationProcessor) {
        DatabaseObjectMetadata metadata = new DatabaseObjectMetadata();
        //set data write config type
        metadata.setDataSaveConfig(annotationProcessor.getModelDataWriteConfiguration());

        LinkedHashMap<String, DatabasePropertyData> propertyDataMap = new LinkedHashMap<>();
        Set<FieldData> fieldDataSet = annotationProcessor.getModelFieldsData();
        fieldDataSet.forEach(data -> {
            DatabasePropertyData databasePropertyData = new DatabasePropertyData();
            databasePropertyData.setNullable(data.isNullable());
            Map<String, String> extraData = data.getExtraData();
            //set old name to null as it is first time creating this metadata
            extraData.put(Constants.METADATA_FIELD_OLD_NAME, null);
            databasePropertyData.setExtraData(extraData);
            propertyDataMap.put(data.getName(), databasePropertyData);
        });
        metadata.setPropertyDataMap(propertyDataMap);
        return metadata;
    }

    private static DatabaseObjectMetadata createFromExistingMetadata(DatabaseObjectMetadata metadata, AnnotationProcessor annotationProcessor) {
        DatabaseObjectMetadata newMetadata = new DatabaseObjectMetadata();
        newMetadata.setDataSaveConfig(annotationProcessor.getModelDataWriteConfiguration());
        LinkedHashMap<String, DatabasePropertyData> propertyDataMap = new LinkedHashMap<>();
        Map<String, DatabasePropertyData> existingProperties = metadata.getPropertyDataMap();
        Set<FieldData> fieldDataSet = annotationProcessor.getModelFieldsData();
        for (FieldData fieldData : fieldDataSet) {
            DatabasePropertyData databasePropertyData = new DatabasePropertyData();
            databasePropertyData.setNullable(fieldData.isNullable());
            Map<String, String> extraData = fieldData.getExtraData();

            Optional<Map.Entry<String, DatabasePropertyData>> existingData = existingProperties.entrySet().stream().toList().stream().filter(p -> {
                String fieldName = p.getValue().getExtraData().get(Constants.METADATA_FIELD_NAME);
                String fieldDataFieldName = fieldData.getExtraData().get(Constants.METADATA_FIELD_NAME);
                return Objects.equals(fieldName, fieldDataFieldName);
            }).collect(Collectors.toSet()).stream().findFirst();

            if (existingData.isEmpty()) {
                extraData.put(Constants.METADATA_FIELD_OLD_NAME, null);
            } else {
                if (!fieldData.getName().equals(existingData.get().getKey())) {
                    extraData.put(Constants.METADATA_FIELD_OLD_NAME, existingData.get().getKey());
                } else {
                    extraData.put(Constants.METADATA_FIELD_OLD_NAME, null);
                }
            }
            databasePropertyData.setExtraData(extraData);
            propertyDataMap.put(fieldData.getName(), databasePropertyData);
        }

        newMetadata.setPropertyDataMap(propertyDataMap);
        return newMetadata;
    }
}
