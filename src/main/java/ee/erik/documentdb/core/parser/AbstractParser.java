package ee.erik.documentdb.core.parser;

import com.google.gson.internal.LinkedTreeMap;
import ee.erik.documentdb.core.metadata.AnnotationProcessor;
import ee.erik.documentdb.core.metadata.DatabaseObjectMetadata;
import lombok.Getter;

import java.io.File;
import java.io.IOException;
import java.util.List;

public abstract class AbstractParser<T> {

    @Getter
    private final DatabaseObjectMetadata metadata;

    @Getter
    private final AnnotationProcessor annotationProcessor;

    /**
     * With empty constructor it is not possible to write Lists due to metadata and annotation processor being null
     */
    public AbstractParser() {
        this.annotationProcessor = null;
        this.metadata = null;
    }

    public AbstractParser(AnnotationProcessor annotationProcessor, DatabaseObjectMetadata metadata) {
        this.annotationProcessor = annotationProcessor;
        this.metadata = metadata;
    }

    /**
     * Due to writeArrayAsData method depends on annotation processor and metadata,
     * it is not possible to use this method when parser is initialized with empty constructor
     * @param objects Array to write
     * @param file File
     */
    public abstract void writeArrayAsData(List<T> objects, File file) throws IOException;

    /**
     * Due to readDataAsList method depends on annotation processor and metadata,
     * it is not possible to use this method when parser is initialized with empty constructor
     * @param file File
     * @return Array of objects from data
     */
    public abstract List<T> readDataAsList(File file) throws IOException;


    /**
     * This method does not use adapter, only useful for simpler objects
     * that don't have custom database annotations
     * @param file
     * @param objectType
     * @return
     * @throws IOException
     */
    public abstract T readDataAsObject(File file, Class<T> objectType) throws IOException;

    /**
     * This method does not use adapter, only useful for simpler objects
     * that don't have custom database annotations
     * @param object
     * @param file
     * @throws IOException
     */
    public abstract void writeObjectAsData(T object, File file) throws IOException;

    /**
     * This method uses parser object adapter to read custom data
     * Also requires object type of T to convert from json to java object,
     * otherwise it will just return LinkedTreeMap
     * @param file
     * @return
     * @throws IOException
     */
    public abstract T readDataAsObjectWithAdapter(File file, Class<T> objectType) throws IOException;

    /**
     * This method uses parser object adapter to write custom data
     * @param object
     * @param file
     * @throws IOException
     */
    public abstract void writeObjectAsDataWithAdapter(T object, File file) throws IOException;

    /**
     * Write map as json
     * only useful for changing data directly
     * @param objectMap
     * @return
     */
    public abstract void writeRawAsDataArray(List<LinkedTreeMap<String, Object>> objectMap, File file) throws IOException;

    public abstract List<LinkedTreeMap<String, Object>> readDataAsRawArray(File file) throws IOException;

    public abstract void writeRawAsDataObject(LinkedTreeMap<String, Object> objectMap, File file) throws IOException;

    public abstract LinkedTreeMap<String, Object> readDataAsRawObject(File file) throws IOException;
}
