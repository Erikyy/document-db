package ee.erik.documentdb.core.parser;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;
import ee.erik.documentdb.core.metadata.AnnotationProcessor;
import ee.erik.documentdb.core.metadata.DatabaseObjectMetadata;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class JsonParser<T> extends AbstractParser<T> {

    public JsonParser() {
        super();
    }

    public JsonParser(AnnotationProcessor annotationProcessor, DatabaseObjectMetadata metadata) {
        super(annotationProcessor, metadata);
    }

    public void writeArrayAsData(List<T> objects, File file) throws IOException {
        Gson gson = new GsonBuilder()
                .registerTypeAdapterFactory(ParserListAdapter.PARSER_LIST_FACTORY(super.getAnnotationProcessor(), super.getMetadata()))
                .create();
        FileWriter fileWriter = new FileWriter(file);
        gson.toJson(objects, fileWriter);
        fileWriter.close();
    }

    public List<T> readDataAsList(File file) throws IOException {
        Gson gson = new GsonBuilder()
                .registerTypeAdapterFactory(ParserListAdapter.PARSER_LIST_FACTORY(super.getAnnotationProcessor(), super.getMetadata()))
                .create();

        FileReader fileReader = new FileReader(file);
        List<T> data =  gson.fromJson(fileReader, new TypeToken<>() {});
        fileReader.close();
        return data;
    }

    /**
     * This method does not support annotations of model.
     *
     * Reads object from data
     * @param file
     * @return
     */
    public T readDataAsObject(File file, Class<T> objectType) throws IOException {
        Gson gson = new GsonBuilder()
                .serializeNulls()
                .setPrettyPrinting()
                .create();

        FileReader fileReader = new FileReader(file);
        T metadata = gson.fromJson(fileReader, objectType);
        fileReader.close();
        return metadata;
    }

    @Override
    public void writeObjectAsData(T object, File file) throws IOException {
        Gson gson = new GsonBuilder()
                .serializeNulls()
                .setPrettyPrinting()
                .create();

        FileWriter writer = new FileWriter(file);
        gson.toJson(object, writer);
        writer.close();
    }

    @Override
    public T readDataAsObjectWithAdapter(File file, Class<T> objectType) throws IOException {
        Gson gson = new GsonBuilder()
                .registerTypeAdapterFactory(ParserObjectAdapter.PARSER_OBJECT_FACTORY(super.getAnnotationProcessor(), super.getMetadata()))
                .create();

        FileReader fileReader = new FileReader(file);
        T data = gson.fromJson(fileReader, objectType);

        fileReader.close();
        return data;
    }

    @Override
    public void writeObjectAsDataWithAdapter(T object, File file) throws IOException {
        Gson gson = new GsonBuilder()
                .serializeNulls()
                .registerTypeAdapterFactory(ParserObjectAdapter.PARSER_OBJECT_FACTORY(this.getAnnotationProcessor(), this.getMetadata()))
                .setPrettyPrinting()
                .create();

        FileWriter writer = new FileWriter(file);
        gson.toJson(object, writer);
        writer.close();
    }

    /**
     * Write map as json
     * only useful for migrations
     * @param objectMap
     * @return
     */
    public void writeRawAsDataArray(List<LinkedTreeMap<String, Object>> objectMap, File file) throws IOException {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .serializeNulls()
                .create();

        FileWriter fileWriter = new FileWriter(file);
        gson.toJson(objectMap, fileWriter);
        fileWriter.close();
    }

    public List<LinkedTreeMap<String, Object>> readDataAsRawArray(File file) throws IOException {
        Gson gson = new GsonBuilder()
                .create();
        FileReader fileReader = new FileReader(file);
        List<LinkedTreeMap<String, Object>> data = gson.fromJson(fileReader, new TypeToken<>(){});
        fileReader.close();
        return data;
    }

    @Override
    public void writeRawAsDataObject(LinkedTreeMap<String, Object> objectMap, File file) throws IOException {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .serializeNulls()
                .create();

        FileWriter fileWriter = new FileWriter(file);
        gson.toJson(objectMap, fileWriter);
        fileWriter.close();
    }

    @Override
    public LinkedTreeMap<String, Object> readDataAsRawObject(File file) throws IOException {
        Gson gson = new GsonBuilder()
                .create();
        FileReader fileReader = new FileReader(file);
        LinkedTreeMap<String, Object> data = gson.fromJson(fileReader, new TypeToken<>(){});
        fileReader.close();
        return data;
    }
}
