package ee.erik.documentdb.core.parser;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import ee.erik.documentdb.core.Constants;
import ee.erik.documentdb.core.metadata.AnnotationProcessor;
import ee.erik.documentdb.core.metadata.DatabaseObjectMetadata;
import ee.erik.documentdb.core.metadata.DatabasePropertyData;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ParserObjectAdapter<T> extends TypeAdapter<T> {

    public static TypeAdapterFactory PARSER_OBJECT_FACTORY(AnnotationProcessor annotationProcessor, DatabaseObjectMetadata metadata) {
        return new TypeAdapterFactory() {
            @Override
            public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {
                return new ParserObjectAdapter<>(annotationProcessor, metadata);
            }
        };
    }
    private final DatabaseObjectMetadata metadata;

    private final AnnotationProcessor annotationProcessor;

    public ParserObjectAdapter(AnnotationProcessor annotationProcessor, DatabaseObjectMetadata metadata) {
        this.annotationProcessor = annotationProcessor;
        this.metadata = metadata;

    }

    @Override
    public void write(JsonWriter out, T value) throws IOException {
        Map<String, DatabasePropertyData> propertyDataMap = this.metadata.getPropertyDataMap();
        ParserUtil.writeObject(propertyDataMap, out, value);
    }

    /**
     * This function here is quite magical, it uses annotation processor to create new instance of T object
     * @param in
     * @return
     * @throws IOException
     */
    @Override
    public T read(JsonReader in) throws IOException {
        return ParserUtil.readObject(in, this.annotationProcessor, this.metadata);
    }
}
