package ee.erik.documentdb.core.parser;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import ee.erik.documentdb.core.Constants;
import ee.erik.documentdb.core.metadata.AnnotationProcessor;
import ee.erik.documentdb.core.metadata.DatabaseObjectMetadata;
import ee.erik.documentdb.core.metadata.DatabasePropertyData;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ParserUtil {

    public static <T> void writeObject(Map<String, DatabasePropertyData> propertyDataMap, JsonWriter out, T value) throws IOException {
        Map<String, Object> values = AnnotationProcessor.getObjectValues(value);
        out.beginObject();
        for (Map.Entry<String, DatabasePropertyData> property : propertyDataMap.entrySet()) {
            out.name(property.getKey());
            for (String key : values.keySet()) {
                //if extraData has this field name
                if (property.getValue().getExtraData().get(Constants.METADATA_FIELD_NAME).equals(key)) {
                    String type = property.getValue().getExtraData().get(Constants.METADATA_FIELD_TYPE);
                    //type cast object to value
                    if (values.get(key) == null) {
                        //if any value is null then just put null as value in json
                        out.nullValue();
                    } else {
                        switch (type) {
                            case "Long", "long" -> {
                                out.value((Long) values.get(key));
                            }
                            case "String" -> {
                                out.value((String) values.get(key));
                            }
                            case "int", "Integer" -> {
                                out.value((Integer) values.get(key));
                            }
                            case "double", "Double" -> {
                                out.value((Double)values.get(key));
                            }
                            case "boolean", "Boolean" -> {
                                out.value((Boolean) values.get(key));
                            }
                            case "List" -> {
                                //write array to json
                                //it can be only list of primitive types
                                out.beginArray();
                                List<?> listValues = (List<?>) values.get(key);
                                for (Object listValue : listValues) {
                                    if (listValue instanceof Long) {
                                        out.value((Long) listValue);
                                    } else if (listValue instanceof Integer) {
                                        out.value((Integer) listValue);
                                    } else if (listValue instanceof String) {
                                        out.value((String) listValue);
                                    } else if (listValue instanceof Double) {
                                        out.value((Double) listValue);
                                    } else if (listValue instanceof Boolean) {
                                        out.value((Boolean) listValue);
                                    }
                                }
                                out.endArray();
                            }
                        }
                    }
                    break;
                }
            }
        }
        out.endObject();
    }

    public static <T> T readObject(JsonReader in, AnnotationProcessor annotationProcessor, DatabaseObjectMetadata metadata) throws IOException {
        T instanceOfT = annotationProcessor.newInstanceOfT();
        in.beginObject();
        while (in.hasNext()) {
            String name = in.nextName();
            DatabasePropertyData databasePropertyData = metadata.getPropertyDataMap().get(name);
            if (databasePropertyData != null) {
                switch (databasePropertyData.getExtraData().get(Constants.METADATA_FIELD_TYPE)) {
                    case "Long", "long" -> {
                        AnnotationProcessor.setFieldValue(databasePropertyData.getExtraData().get(Constants.METADATA_FIELD_NAME), in.nextLong(), instanceOfT);
                    }
                    case "String" -> {
                        AnnotationProcessor.setFieldValue(databasePropertyData.getExtraData().get(Constants.METADATA_FIELD_NAME), in.nextString(), instanceOfT);
                    }
                    case "int", "Integer" -> {
                        AnnotationProcessor.setFieldValue(databasePropertyData.getExtraData().get(Constants.METADATA_FIELD_NAME), in.nextInt(), instanceOfT);
                    }
                    case "double", "Double" -> {
                        AnnotationProcessor.setFieldValue(databasePropertyData.getExtraData().get(Constants.METADATA_FIELD_NAME), in.nextDouble(), instanceOfT);
                    }
                    case "boolean", "Boolean" -> {
                        AnnotationProcessor.setFieldValue(databasePropertyData.getExtraData().get(Constants.METADATA_FIELD_NAME), in.nextBoolean(), instanceOfT);
                    }
                    case "List" -> {
                        //here references must be
                        //here just parse normal lists
                        List<Object> list = new ArrayList<>();
                        String type = databasePropertyData.getExtraData().get(Constants.METADATA_FIELD_SUB_TYPE);
                        in.beginArray();
                        while (in.hasNext()) {
                            if (type.equals(Integer.class.getSimpleName())) {
                                list.add(in.nextInt());
                            } else if (type.equals(String.class.getSimpleName())) {
                                list.add(in.nextString());
                            } else if (type.equals(Double.class.getSimpleName())) {
                                list.add(in.nextDouble());
                            } else if (type.equals(Boolean.class.getSimpleName())) {
                                list.add(in.nextBoolean());
                            } else if (type.equals(Long.class.getSimpleName())) {
                                list.add(in.nextLong());
                            }
                        }
                        in.endArray();
                        AnnotationProcessor.setFieldValue(databasePropertyData.getExtraData().get(Constants.METADATA_FIELD_NAME), list, instanceOfT);
                    }
                }
            } else {
                in.nextNull();
            }

        }
        in.endObject();

        return instanceOfT;
    }
}
