package ee.erik.documentdb.core.parser;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;
import ee.erik.documentdb.core.metadata.AnnotationProcessor;
import ee.erik.documentdb.core.metadata.DatabaseObjectMetadata;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class YamlParser<T> extends AbstractParser<T> {

    public YamlParser() {
        super();
    }

    public YamlParser(AnnotationProcessor annotationProcessor, DatabaseObjectMetadata metadata) {
        super(annotationProcessor, metadata);
    }

    @Override
    public void writeArrayAsData(List<T> objects, File file) throws IOException {
        Yaml yaml = new Yaml();

        FileWriter fileWriter = new FileWriter(file);
        List<LinkedTreeMap<String, Object>> rawData = this.toRawMap(objects);
        yaml.dump(rawData, fileWriter);
        fileWriter.close();

    }

    @Override
    public List<T> readDataAsList(File file) throws IOException {
        Yaml yaml = new Yaml();

        FileReader fileReader = new FileReader(file);
        List<Map<String, Object>> yamlData = yaml.load(fileReader);
        fileReader.close();
        return this.fromRawToList(yamlData);
    }

    @Override
    public T readDataAsObject(File file, Class<T> objectType) throws IOException {
        Yaml yaml = new Yaml();

        FileReader reader = new FileReader(file);
        T data = yaml.loadAs(reader, objectType);
        reader.close();
        return data;

    }

    @Override
    public void writeObjectAsData(T object, File file) throws IOException {
        Yaml yaml = new Yaml();
        FileWriter writer = new FileWriter(file);
        yaml.dump(object, writer);
        writer.close();
    }

    @Override
    public T readDataAsObjectWithAdapter(File file, Class<T> objectType) throws IOException {
        Yaml yaml = new Yaml();

        FileReader fileReader = new FileReader(file);
        Map<String, Object> yamlData = yaml.load(fileReader);
        fileReader.close();
        return this.fromRawObject(yamlData, objectType);
    }

    @Override
    public void writeObjectAsDataWithAdapter(T object, File file) throws IOException {
        Yaml yaml = new Yaml();

        FileWriter fileWriter = new FileWriter(file);
        LinkedTreeMap<String, Object> rawData = this.toRawObject(object);
        yaml.dump(rawData, fileWriter);
        fileWriter.close();
    }

    @Override
    public void writeRawAsDataArray(List<LinkedTreeMap<String, Object>> objectMap, File file) throws IOException {
        Yaml yaml = new Yaml();

        FileWriter fileWriter = new FileWriter(file);
        yaml.dump(objectMap, fileWriter);
        fileWriter.close();

    }

    @Override
    public List<LinkedTreeMap<String, Object>> readDataAsRawArray(File file) throws IOException {
        Gson gson = new GsonBuilder()
                .create();
        Yaml yaml = new Yaml();

        FileReader fileReader = new FileReader(file);
        Object yamlData = yaml.load(fileReader);
        fileReader.close();
        String jsonData = gson.toJson(yamlData);
        return gson.fromJson(jsonData, new TypeToken<>(){});
    }

    @Override
    public void writeRawAsDataObject(LinkedTreeMap<String, Object> objectMap, File file) throws IOException {
        Yaml yaml = new Yaml();
        FileWriter fileWriter = new FileWriter(file);
        yaml.dump(objectMap, fileWriter);
        fileWriter.close();
    }

    @Override
    public LinkedTreeMap<String, Object> readDataAsRawObject(File file) throws IOException {
        Gson gson = new GsonBuilder()
                .create();
        Yaml yaml = new Yaml();

        FileReader fileReader = new FileReader(file);
        Object yamlData = yaml.load(fileReader);
        fileReader.close();
        String jsonData = gson.toJson(yamlData);
        LinkedTreeMap<String, Object> data = gson.fromJson(jsonData, new TypeToken<>(){});
        fileReader.close();
        return data;
    }

    private List<LinkedTreeMap<String, Object>> toRawMap(List<T> objects) {
        Gson gson = new GsonBuilder()
                .registerTypeAdapterFactory(ParserListAdapter.PARSER_LIST_FACTORY(super.getAnnotationProcessor(), super.getMetadata()))
                .create();
        String json = gson.toJson(objects);
        Gson rawGson = new GsonBuilder().create();
        return rawGson.fromJson(json, new TypeToken<>(){});
    }

    /**
     * Takes raw list map data and converts it into List<T>
     *
     * @param data
     * @return
     */
    private List<T> fromRawToList(List<Map<String, Object>> data) {
        Gson gson = new GsonBuilder()
                .registerTypeAdapterFactory(ParserListAdapter.PARSER_LIST_FACTORY(super.getAnnotationProcessor(), super.getMetadata()))
                .create();
        Gson rawGson = new GsonBuilder().create();
        String jsonData = rawGson.toJson(data);
        return gson.fromJson(jsonData, new TypeToken<>(){});
    }

    private LinkedTreeMap<String, Object> toRawObject(T data) {
        Gson gson = new GsonBuilder()
                .registerTypeAdapterFactory(ParserObjectAdapter.PARSER_OBJECT_FACTORY(super.getAnnotationProcessor(), super.getMetadata()))
                .create();
        String jsonData = gson.toJson(data);
        Gson rawGson = new GsonBuilder().create();
        return rawGson.fromJson(jsonData, new TypeToken<>(){});
    }

    private T fromRawObject(Map<String, Object> data, Class<T> objectType) {
        Gson gson = new GsonBuilder()
                .registerTypeAdapterFactory(ParserObjectAdapter.PARSER_OBJECT_FACTORY(super.getAnnotationProcessor(), super.getMetadata()))
                .create();
        Gson rawGson = new GsonBuilder().create();
        String jsonData = rawGson.toJson(data);
        return gson.fromJson(jsonData, objectType);
    }
}
