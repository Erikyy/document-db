package ee.erik.documentdb.dao;

import java.util.List;
import java.util.Optional;

public interface ListGenericDao<T, ID> {

    void setObjectInfo(Class<T> object);

    T insert(T object);

    T update(T object);

    void delete(T object);

    Optional<T> findById(ID id);

    List<T> findAll();

    /**
     * !!! Warning this method will delete all data in model name directory
     * Usually when archive lifetime is set to Transient or InMemory
     * it will delete folders with no warnings, Persist mode will need override-persist-delete set to true
     * in order to delete data in Persist mode.
     */
    void destroy();
}
