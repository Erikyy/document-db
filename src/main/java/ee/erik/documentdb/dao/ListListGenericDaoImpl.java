package ee.erik.documentdb.dao;

import ee.erik.documentdb.core.ModelDbConfiguration;
import ee.erik.documentdb.core.ModelDbArrayOperations;
import ee.erik.documentdb.properties.DocumentDbProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ListListGenericDaoImpl<T, ID> implements ListGenericDao<T, ID> {


    private DocumentDbProperties properties;

    private ModelDbArrayOperations<T, ID> operations;


    @Autowired
    public void setProperties(DocumentDbProperties properties) {
        this.properties = properties;
    }

    /**
     * Generates object metadata and data
     * @param object
     */
    public void setObjectInfo(Class<T> object) {
        this.operations = new ModelDbArrayOperations<>(
                object,
                new ModelDbConfiguration(
                        this.properties.getBaseFileType(),
                        this.properties.getLifetime(),
                        this.properties.getArchiveName(),
                        this.properties.isOverridePersistDelete()
                )
        );
    }

    @Override
    public T insert(T object) {
         return this.operations.insert(object);
    }

    @Override
    public T update(T object) {
        return this.operations.update(object);
    }

    @Override
    public void delete(T object) {
        this.operations.delete(object);
    }

    @Override
    public Optional<T> findById(ID id) {
        return this.operations.findById(id);
    }

    @Override
    public List<T> findAll() {
        return this.operations.findAll();
    }

    @Override
    public void destroy() {
        this.operations.destroy();
    }
}
