package ee.erik.documentdb.dao;

/**
 * This dao is much simplier than ListGenericDao
 * This is used for writing just objects to database without array
 * @param <T>
 */
public interface ObjectGenericDao<T> {

    void setObjectInfo(Class<T> object);

    T save(T object);

    T getObject();

    /**
     * This method will just set data to null
     */
    void delete();

    /**
     * !!! Warning this method will delete all data in model name directory
     * Usually when archive lifetime is set to Transient or InMemory
     * it will delete folders with no warnings, Persist mode will need override-persist-delete set to true
     * in order to delete data in Persist mode.
     */
    void destroy();
}
