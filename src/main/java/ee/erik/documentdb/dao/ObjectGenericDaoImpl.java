package ee.erik.documentdb.dao;

import ee.erik.documentdb.core.ModelDbConfiguration;
import ee.erik.documentdb.core.ModelDbObjectOperations;
import ee.erik.documentdb.properties.DocumentDbProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ObjectGenericDaoImpl<T> implements ObjectGenericDao<T> {

    private DocumentDbProperties properties;

    private ModelDbObjectOperations<T> operations;

    @Autowired
    public void setProperties(DocumentDbProperties properties) {
        this.properties = properties;
    }

    @Override
    public void setObjectInfo(Class<T> object) {
        this.operations = new ModelDbObjectOperations<>(
                object,
                new ModelDbConfiguration(
                        this.properties.getBaseFileType(),
                        this.properties.getLifetime(),
                        this.properties.getArchiveName(),
                        this.properties.isOverridePersistDelete()
                )
        );
    }

    @Override
    public T save(T object) {
        return this.operations.save(object);
    }

    @Override
    public T getObject() {
        return this.operations.getObject();
    }

    @Override
    public void delete() {
        this.operations.delete();
    }

    @Override
    public void destroy() {
        this.operations.destroy();
    }
}
