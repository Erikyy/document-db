package ee.erik.documentdb.properties;

import ee.erik.documentdb.core.ArchiveLifetime;
import ee.erik.documentdb.core.FileType;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "ee.erik.documentdb")
public class DocumentDbProperties {

    /**
     * Sets archive lifetime.
     * Default is transient.
     */
    @Getter
    @Setter
    private ArchiveLifetime lifetime = ArchiveLifetime.Transient;

    /**
     * Sets base archive folder name.
     * Default value is 'database'
     */
    @Getter
    @Setter
    private String archiveName = "database";

    /**
     * Sets database base file type. This can be overridden using OverrideFileType annotation.
     * Default value is json.
     */
    @Getter
    @Setter
    private FileType baseFileType = FileType.Json;

    /**
     * <h1>!!!Warning!!!</h1> This if this flag is set to true it will delete data in Persist Mode.
     * This flag is set to false by default
     */
    @Getter
    @Setter
    private boolean overridePersistDelete = false;
}
