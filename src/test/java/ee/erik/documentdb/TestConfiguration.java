package ee.erik.documentdb;

import ee.erik.documentdb.config.EnableDocumentDb;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableDocumentDb
public class TestConfiguration {
}
