package ee.erik.documentdb.core;

import ee.erik.documentdb.core.metadata.TestAnnotatedModel;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

public class ModelDbArrayOperationsTest {

    @Test
    public void shouldPerformAllListOperations() {
        ModelDbArrayOperations<TestAnnotatedModel, Long> operations = new ModelDbArrayOperations<>(TestAnnotatedModel.class, new ModelDbConfiguration(
                FileType.Json,
                ArchiveLifetime.Transient,
                "test-db-model",
                false
        ));
        TestAnnotatedModel testAnnotatedModel = new TestAnnotatedModel();
        testAnnotatedModel.setName("Test 1");

        TestAnnotatedModel testAnnotatedModel2 = new TestAnnotatedModel();
        testAnnotatedModel2.setName("Test 2");

        TestAnnotatedModel saved = operations.insert(testAnnotatedModel);
        TestAnnotatedModel saved2 = operations.insert(testAnnotatedModel2);

        assertThat(saved).isNotNull();
        //first element in database so id is assigned 0

        assertThat(saved.getId()).isEqualTo(0);

        assertThat(saved2).isNotNull();
        //should autoincrement id by 1
        assertThat(saved2.getId()).isEqualTo(1);

        List<TestAnnotatedModel> savedData = operations.findAll();

        assertThat(savedData).isNotEmpty();

        Optional<TestAnnotatedModel> foundData = operations.findById(0L);

        assertThat(foundData).isPresent();
        assertThat(foundData.get().getName()).isEqualTo("Test 1");

        TestAnnotatedModel updateData = new TestAnnotatedModel(0L, "Test 2");
        TestAnnotatedModel savedUpdate = operations.update(updateData);

        assertThat(savedUpdate).isEqualTo(updateData);

        operations.delete(new TestAnnotatedModel(0L, "Test 2"));
        operations.delete(new TestAnnotatedModel(1L, "Test 2"));

        List<TestAnnotatedModel> data = operations.findAll();

        assertThat(data).isEmpty();

        operations.destroy();
    }
}
