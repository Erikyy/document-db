package ee.erik.documentdb.core;

import ee.erik.documentdb.core.metadata.TestAnnotatedModel;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

public class ModelDbEngineTest {

    @Test
    public void shouldCreateModelDbEngineAndPerformReadAndWriteOperationsInJsonFormat() {
        ModelDbEngine<TestAnnotatedModel> engine = new ModelDbEngine<>(TestAnnotatedModel.class,
                new ModelDbConfiguration(
                        FileType.Json,
                        ArchiveLifetime.Transient,
                        "test-db",
                        false
                ));

        assertDoesNotThrow(() -> {
            this.performOperations(engine);
        });

        engine.destroy();
    }

    @Test
    public void shouldCreateModelDbEngineAndPerformReadAndWriteOperationsInYamlFormat() {
        ModelDbEngine<TestAnnotatedModel> engine = new ModelDbEngine<>(TestAnnotatedModel.class,
                new ModelDbConfiguration(
                        FileType.Yaml,
                        ArchiveLifetime.Transient,
                        "test-db",
                        false
                ));
        assertDoesNotThrow(() -> {
            this.performOperations(engine);
        });
        engine.destroy();
    }


    private void performOperations(ModelDbEngine<TestAnnotatedModel> engine) throws Exception {
        List<TestAnnotatedModel> testAnnotatedModels = new ArrayList<>();
        testAnnotatedModels.add(new TestAnnotatedModel(1L, "Test1"));
        testAnnotatedModels.add(new TestAnnotatedModel(2L, "Test1"));
        testAnnotatedModels.add(new TestAnnotatedModel(3L, "Test1"));
        testAnnotatedModels.add(new TestAnnotatedModel(4L, "Test1"));
        testAnnotatedModels.add(new TestAnnotatedModel(5L, "Test1"));

        engine.writeDataAsArray(testAnnotatedModels);

        List<TestAnnotatedModel> data = engine.readDataAsArray();

        assertThat(data).isNotEmpty();
        assertThat(data).isEqualTo(testAnnotatedModels);
    }

}
