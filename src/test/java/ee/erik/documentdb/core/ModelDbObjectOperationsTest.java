package ee.erik.documentdb.core;

import ee.erik.documentdb.core.metadata.TestModelSingleObject;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class ModelDbObjectOperationsTest {
    @Test
    public void shouldPerformAllObjectOperations() {
        ModelDbObjectOperations<TestModelSingleObject> operations = new ModelDbObjectOperations<>(TestModelSingleObject.class,
                new ModelDbConfiguration(
                        FileType.Yaml,
                        ArchiveLifetime.Transient,
                        "single-db",
                        false
                ));

        TestModelSingleObject model = new TestModelSingleObject("Test 1", "Test 2", 6);

        TestModelSingleObject saved = operations.save(model);

        assertThat(saved).isEqualTo(model);

        saved.setName("Test 2 Update");
        saved.setSomeNumber(90);
        //just saving it will overwrite data
        TestModelSingleObject updated = operations.save(saved);
        assertThat(updated).isEqualTo(saved);

        TestModelSingleObject dbData = operations.getObject();

        assertThat(dbData).isEqualTo(updated);

        operations.delete();

        //after deleting object should be null
        TestModelSingleObject nullData = operations.getObject();
        assertThat(nullData).isNull();
        operations.destroy();

    }
}