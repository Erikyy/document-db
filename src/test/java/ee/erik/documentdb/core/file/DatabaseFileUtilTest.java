package ee.erik.documentdb.core.file;



import ee.erik.documentdb.core.file.FileUtils;
import org.junit.jupiter.api.Test;


import java.io.File;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

public class DatabaseFileUtilTest {

    @Test
    public void databaseFileUtilShouldCreateFoldersAndFiles() {
        FileUtils fileUtils = new FileUtils("test-base")
                .createSubDirectory("test")
                .createFile("test1.txt")
                .createFile("test2.txt");

        File file1 = fileUtils.getFile("test1.txt");
        File file2 = fileUtils.getFile("test2.txt");

        assertThat(file1).exists();
        assertThat(file1).canRead();
        assertThat(file1).canWrite();

        assertThat(file2).exists();
        assertThat(file2).canRead();
        assertThat(file2).canWrite();

        assertDoesNotThrow(fileUtils::delete);

        assertThat(file1).doesNotExist();
        assertThat(file2).doesNotExist();
    }
}
