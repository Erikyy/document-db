package ee.erik.documentdb.core.metadata;

import ee.erik.documentdb.core.Constants;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class AnnotationProcessorTest {


    @Test
    public void shouldInitializeAndGetMetadataOfClass() {
        //here we have not overriden
        AnnotationProcessor annotationProcessor = new AnnotationProcessor(TestAnnotatedModel.class);
        String fileName = annotationProcessor.getModelDataFileName("data");

        //this is not overridden in TestAnnotatedModel
        assertThat(fileName).isEqualTo("data");
        assertThat(annotationProcessor.getModelName()).isEqualTo(TestAnnotatedModel.class.getSimpleName());

        Set<FieldData> fieldDataSet = annotationProcessor.getModelFieldsData();

        //get id field from model
        FieldData fieldData = fieldDataSet.stream().findFirst().orElse(null);
        assertThat(fieldData).isNotNull();
        //does not have custom name
        assertThat(fieldData.getName()).isEqualTo("id");

        //this model has Id and GeneratedId annotations
        assertThat(fieldData.getExtraData().get(Constants.METADATA_ID_DEFINITION)).isNotNull();
        assertThat(fieldData.getExtraData().get(Constants.METADATA_ID_GENERATION_TYPE)).isNotNull();
        assertThat(fieldData.getExtraData().get(Constants.METADATA_ID_GENERATION_TYPE)).isEqualTo(Constants.METADATA_ID_AUTOINCREMENT);
    }

    @Test
    public void shouldGetMetadataFromNonAnnotatedClass() {
        AnnotationProcessor annotationProcessor = new AnnotationProcessor(TestModel.class);
        //should have same stuff
        String fileName = annotationProcessor.getModelDataFileName("data");
        assertThat(fileName).isEqualTo("data");
        assertThat(annotationProcessor.getModelName()).isEqualTo(TestModel.class.getSimpleName());
        Set<FieldData> fieldDataSet = annotationProcessor.getModelFieldsData();

        //get id field from model
        FieldData fieldData = fieldDataSet.stream().findFirst().orElse(null);
        assertThat(fieldData).isNotNull();
        //has first field named id
        assertThat(fieldData.getName()).isEqualTo("id");

    }
}
