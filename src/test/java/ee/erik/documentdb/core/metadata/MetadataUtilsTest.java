package ee.erik.documentdb.core.metadata;

import ee.erik.documentdb.core.Constants;
import ee.erik.documentdb.core.FileType;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.assertj.core.api.Assertions.assertThat;

public class MetadataUtilsTest {


    @Test
    public void shouldGenerateJsonMetadataFromNonAnnotatedModel() throws Exception {
        AnnotationProcessor annotationProcessor = new AnnotationProcessor(TestModel.class);
        File file = new File("metadata.json");
        file.createNewFile();
        DatabaseObjectMetadata metadata = MetadataUtils.generateMetadata(file, annotationProcessor, FileType.Json);

        //assert that it contains property entry named id
        assertThat(metadata.getPropertyDataMap()).containsKey("id");

        DatabasePropertyData propertyData = metadata.getPropertyDataMap().get("id");

        //check if property data has field type and field name
        //anything else shouldn't be stored here because of model that isn't annotated
        assertThat(propertyData.getExtraData()).containsKey(Constants.METADATA_FIELD_TYPE);
        assertThat(propertyData.getExtraData()).containsKey(Constants.METADATA_FIELD_NAME);

        file.delete();
    }

    @Test
    public void shouldGenerateYamlMetadataFromNonAnnotatedModel() throws Exception {
        AnnotationProcessor annotationProcessor = new AnnotationProcessor(TestModel.class);
        File file = new File("metadata.yaml");
        file.createNewFile();
        DatabaseObjectMetadata metadata = MetadataUtils.generateMetadata(file, annotationProcessor, FileType.Yaml);

        //assert that it contains property entry named id
        assertThat(metadata.getPropertyDataMap()).containsKey("id");

        DatabasePropertyData propertyData = metadata.getPropertyDataMap().get("id");

        //check if property data has field type and field name
        //anything else shouldn't be stored here because of model that isn't annotated
        assertThat(propertyData.getExtraData()).containsKey(Constants.METADATA_FIELD_TYPE);
        assertThat(propertyData.getExtraData()).containsKey(Constants.METADATA_FIELD_NAME);
        file.delete();
    }

    @Test
    public void shouldGenerateJsonMetadataFromAnnotatedModel() throws Exception {
        AnnotationProcessor annotationProcessor = new AnnotationProcessor(TestAnnotatedModel.class);
        File file = new File("metadata.json");
        file.createNewFile();
        DatabaseObjectMetadata metadata = MetadataUtils.generateMetadata(file, annotationProcessor, FileType.Json);

        //assert that it contains property entry named id
        assertThat(metadata.getPropertyDataMap()).containsKey("id");

        DatabasePropertyData propertyData = metadata.getPropertyDataMap().get("id");

        //check if property data has field type and field name
        //anything else shouldn't be stored here because of model that isn't annotated
        assertThat(propertyData.getExtraData()).containsKey(Constants.METADATA_FIELD_TYPE);
        assertThat(propertyData.getExtraData()).containsKey(Constants.METADATA_FIELD_NAME);
        assertThat(propertyData.getExtraData()).containsKey(Constants.METADATA_ID_DEFINITION);
        assertThat(propertyData.getExtraData()).containsKey(Constants.METADATA_ID_GENERATION_TYPE);

        file.delete();
    }

    @Test
    public void shouldGenerateYamlMetadataFromAnnotatedModel() throws Exception {
        AnnotationProcessor annotationProcessor = new AnnotationProcessor(TestAnnotatedModel.class);
        File file = new File("metadata.yaml");
        file.createNewFile();
        DatabaseObjectMetadata metadata = MetadataUtils.generateMetadata(file, annotationProcessor, FileType.Yaml);

        //assert that it contains property entry named id
        assertThat(metadata.getPropertyDataMap()).containsKey("id");

        DatabasePropertyData propertyData = metadata.getPropertyDataMap().get("id");

        //check if property data has field type and field name
        assertThat(propertyData.getExtraData()).containsKey(Constants.METADATA_FIELD_TYPE);
        assertThat(propertyData.getExtraData()).containsKey(Constants.METADATA_FIELD_NAME);
        assertThat(propertyData.getExtraData()).containsKey(Constants.METADATA_ID_DEFINITION);
        assertThat(propertyData.getExtraData()).containsKey(Constants.METADATA_ID_GENERATION_TYPE);

        file.delete();
    }
}
