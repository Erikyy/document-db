package ee.erik.documentdb.core.metadata;

import ee.erik.documentdb.core.annotations.GeneratedId;
import ee.erik.documentdb.core.annotations.GenerationType;
import ee.erik.documentdb.core.annotations.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TestAnnotatedModel {
    @Id
    @GeneratedId(generationType = GenerationType.AutoIncrement)
    private Long id;

    private String name;
}
