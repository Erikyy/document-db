package ee.erik.documentdb.core.metadata;


import lombok.Data;

/**
 * This model is just pure java class
 * Annotation processor should generate field data even for this class
 */
@Data
public class TestModel {
    private Long id;
}
