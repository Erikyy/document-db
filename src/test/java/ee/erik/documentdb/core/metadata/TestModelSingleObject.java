package ee.erik.documentdb.core.metadata;

import ee.erik.documentdb.core.annotations.DatabaseObjectAsSingleton;
import ee.erik.documentdb.core.annotations.ObjectProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * This model demonstrates usage of DatabaseObjectAsSingle annotation,
 * which stores only this object data to database in object format instead of array
 */
@DatabaseObjectAsSingleton
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TestModelSingleObject {
    @ObjectProperty(name = "test_object_name")
    private String name;

    @ObjectProperty(name = "test_some_other_name")
    private String someOtherField;

    @ObjectProperty(name = "test_some_number")
    private int someNumber;
}
