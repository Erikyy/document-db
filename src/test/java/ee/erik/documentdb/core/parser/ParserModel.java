package ee.erik.documentdb.core.parser;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Parser should be able to write and read sub lists
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ParserModel {
    private String name;
    private List<String> stuff;
}
