package ee.erik.documentdb.core.parser;

import ee.erik.documentdb.core.FileType;
import ee.erik.documentdb.core.metadata.AnnotationProcessor;
import ee.erik.documentdb.core.metadata.DatabaseObjectMetadata;
import ee.erik.documentdb.core.metadata.MetadataUtils;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
/**
 * Tests yaml and json parser
 */
public class ParserTest {

    private AnnotationProcessor annotationProcessor;

    @BeforeEach
    public void setup() {
        this.annotationProcessor = new AnnotationProcessor(ParserModel.class);
    }


    @Test
    public void shouldWriteToJsonFileAndReturnData() throws Exception {
        File metadataFile = new File("metadata.json");
        metadataFile.createNewFile();
        DatabaseObjectMetadata metadata = MetadataUtils.generateMetadata(metadataFile, annotationProcessor, FileType.Json);
        //parser needs annotation processor and metadata so here creates new metadata in json format
        AbstractParser<ParserModel> parser = new JsonParser<>(annotationProcessor, metadata);
        File dataFile = new File("data.json");
        dataFile.createNewFile();


        //test list writing and reading
        List<ParserModel> testList = new ArrayList<>();
        ParserModel parserModel = new ParserModel("test1", List.of("test", "test"));
        testList.add(parserModel);
        testList.add(new ParserModel("test2", List.of("test2", "test2", "test2")));

        parser.writeArrayAsData(testList, dataFile);

        List<ParserModel> readDataList = parser.readDataAsList(dataFile);
        Assertions.assertThat(readDataList).isNotEmpty();
        Assertions.assertThat(readDataList).contains(parserModel);

        metadataFile.delete();
        dataFile.delete();

    }

    @Test
    public void shouldWriteToYamlFileAndReturnData() throws Exception {
        File metadataFile = new File("metadata.json");
        metadataFile.createNewFile();
        DatabaseObjectMetadata metadata = MetadataUtils.generateMetadata(metadataFile, this.annotationProcessor, FileType.Yaml);
        //parser needs annotation processor and metadata so here creates new metadata in json format
        AbstractParser<ParserModel> parser = new YamlParser<>(this.annotationProcessor, metadata);
        File dataFile = new File("data.yaml");
        dataFile.createNewFile();


        //test list writing and reading
        List<ParserModel> testList = new ArrayList<>();
        ParserModel parserModel = new ParserModel("test1", List.of("test", "test"));
        testList.add(parserModel);
        testList.add(new ParserModel("test2", List.of("test2", "test2", "test2")));

        parser.writeArrayAsData(testList, dataFile);

        List<ParserModel> readDataList = parser.readDataAsList(dataFile);
        Assertions.assertThat(readDataList).isNotEmpty();
        Assertions.assertThat(readDataList).contains(parserModel);

        metadataFile.delete();
        dataFile.delete();
    }

    @Test
    public void shouldWriteAndReadJsonObjectWithoutAdapter() throws Exception {
        File dataFile = new File("data.json");
        dataFile.createNewFile();
        AbstractParser<ParserModel> parser = new JsonParser<>();

        ParserModel parserModel = new ParserModel();
        parserModel.setName("Test");
        parserModel.setStuff(List.of("Test 1, Test 2"));

        parser.writeObjectAsData(parserModel, dataFile);

        ParserModel readParserModel = parser.readDataAsObject(dataFile, ParserModel.class);

        assertThat(readParserModel).isEqualTo(parserModel);
    }

    @Test
    public void shouldWriteAndReadJsonObjectWithAdapter() throws Exception {
        File dataFile = new File("data.json");
        dataFile.createNewFile();
        File metadataFile = new File("metadata.json");
        metadataFile.createNewFile();
        DatabaseObjectMetadata metadata = MetadataUtils.generateMetadata(metadataFile, this.annotationProcessor, FileType.Json);
        AbstractParser<ParserModel> parser = new JsonParser<>(this.annotationProcessor, metadata);

        ParserModel parserModel = new ParserModel();
        parserModel.setName("Test");
        parserModel.setStuff(List.of("Test 1, Test 2"));

        parser.writeObjectAsDataWithAdapter(parserModel, dataFile);

        ParserModel readParserModel = parser.readDataAsObjectWithAdapter(dataFile, ParserModel.class);

        assertThat(readParserModel).isEqualTo(parserModel);
    }

    @Test
    public void shouldWriteAndReadYamlObjectWithoutAdapter() throws Exception {
        File dataFile = new File("data.yaml");
        dataFile.createNewFile();
        AbstractParser<ParserModel> parser = new YamlParser<>();

        ParserModel parserModel = new ParserModel();
        parserModel.setName("Test");
        parserModel.setStuff(List.of("Test 1, Test 2"));

        parser.writeObjectAsData(parserModel, dataFile);

        ParserModel readParserModel = parser.readDataAsObject(dataFile, ParserModel.class);

        assertThat(readParserModel).isEqualTo(parserModel);

        dataFile.delete();
    }

    @Test
    public void shouldWriteAndReadYamlObjectWithAdapter() throws Exception {
        File dataFile = new File("data.yaml");
        dataFile.createNewFile();
        File metadataFile = new File("metadata.yaml");
        metadataFile.createNewFile();
        DatabaseObjectMetadata metadata = MetadataUtils.generateMetadata(metadataFile, this.annotationProcessor, FileType.Yaml);
        AbstractParser<ParserModel> parser = new YamlParser<>(this.annotationProcessor, metadata);

        ParserModel parserModel = new ParserModel();
        parserModel.setName("Test");
        parserModel.setStuff(List.of("Test 1, Test 2"));

        parser.writeObjectAsDataWithAdapter(parserModel, dataFile);

        ParserModel readParserModel = parser.readDataAsObjectWithAdapter(dataFile, ParserModel.class);

        assertThat(readParserModel).isEqualTo(parserModel);

        dataFile.delete();
        metadataFile.delete();
    }

}
