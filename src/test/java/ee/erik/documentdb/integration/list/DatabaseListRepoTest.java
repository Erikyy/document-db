package ee.erik.documentdb.integration.list;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;


@SpringBootTest
public class DatabaseListRepoTest {

    @Autowired
    private TestRepository testRepository;

    @Autowired
    private Test2Repository test2Repository;

    @Test
    public void runTests() {
        TestModel testModel = new TestModel();
        testModel.setName("Test");
        testModel.setTest2(new ArrayList<>());
        testModel.setSomePrimitiveArray(List.of("hello", "hello", "hello"));
        TestModel testModelSecond = new TestModel();
        testModelSecond.setName("Test");
        testModelSecond.setSomePrimitiveArray(List.of("hello2", "hello2", "hello2"));

        TestModel2 testModel2 = new TestModel2();
        testModel2.setName("Test 2");

        TestModel2 savedTest21 = this.test2Repository.insert(testModel2);
        TestModel2 savedTest22 = this.test2Repository.insert(testModel2);

        assertThat(savedTest21).isNotNull();
        assertThat(savedTest22).isNotNull();

        testModelSecond.setTest2(List.of(savedTest21.getId()));
        TestModel saved1 = this.testRepository.insert(testModel);
        TestModel saved2 = this.testRepository.insert(testModelSecond);

        assertThat(saved1).isNotNull();
        assertThat(saved2).isNotNull();


        List<TestModel> testModels = this.testRepository.findAll();
        assertThat(testModels).isNotEmpty();
        List<TestModel2> testModel2s = this.test2Repository.findAll();
        assertThat(testModel2s).isNotEmpty();

        this.testRepository.delete(testModelSecond);

        Optional<TestModel> updateModel = this.testRepository.findById(0L);
        if (updateModel.isPresent()) {
            updateModel.get().setName("Updated name at 0");
            TestModel updatedModel = this.testRepository.update(updateModel.get());
            assertThat(updatedModel).isNotNull();
            assertThat(updatedModel.getName()).isEqualTo("Updated name at 0");
        }

    }
}
