package ee.erik.documentdb.integration.list;

import java.util.List;

public interface Test2Repository {
    TestModel2 insert(TestModel2 testModel);

    TestModel2 update(TestModel2 testModel);
    void delete(TestModel2 testModel);

    List<TestModel2> findAll();
}
