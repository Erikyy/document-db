package ee.erik.documentdb.integration.list;

import ee.erik.documentdb.dao.ListGenericDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.PreDestroy;
import java.util.List;

@Repository
public class Test2RepositoryImpl implements Test2Repository {
    private ListGenericDao<TestModel2, Long> dao;

    @Autowired
    public void setDao(ListGenericDao<TestModel2, Long> dao) {
        this.dao = dao;
        dao.setObjectInfo(TestModel2.class);
    }

    @Override
    public TestModel2 insert(TestModel2 testModel) {
        return this.dao.insert(testModel);
    }

    @Override
    public TestModel2 update(TestModel2 testModel) {
        return this.dao.update(testModel);
    }

    @Override
    public void delete(TestModel2 testModel) {
        this.dao.delete(testModel);
    }

    @Override
    public List<TestModel2> findAll() {
        return this.dao.findAll();
    }

    @PreDestroy
    public void destroyData() {
        this.dao.destroy();
    }
}
