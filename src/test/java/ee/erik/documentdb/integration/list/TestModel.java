package ee.erik.documentdb.integration.list;

import ee.erik.documentdb.core.FileType;
import ee.erik.documentdb.core.annotations.*;
import lombok.Data;

import java.util.List;

@Data
@DatabaseObject(name = "test")
@OverrideFileType(fileType = FileType.Yaml)
public class TestModel {

    @Id
    @GeneratedId(generationType = GenerationType.AutoIncrement)
    @ObjectProperty(name = "p_test_id")
    private Long id;

    @ObjectProperty(name = "p_test_name")
    private String name;

    @ObjectProperty(name = "p_test_2")
    private List<Long> test2;

    private List<String> somePrimitiveArray;
}
