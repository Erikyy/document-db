package ee.erik.documentdb.integration.list;

import ee.erik.documentdb.core.annotations.*;
import lombok.Data;

@Data
@DatabaseObject
public class TestModel2 {
    @Id
    @GeneratedId(generationType = GenerationType.AutoIncrement)
    private Long id;

    @ObjectProperty(name = "test_2_name")
    private String name;
}
