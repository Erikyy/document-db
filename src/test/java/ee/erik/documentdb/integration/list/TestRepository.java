package ee.erik.documentdb.integration.list;

import java.util.List;
import java.util.Optional;

public interface TestRepository {

    TestModel insert(TestModel testModel);

    TestModel update(TestModel testModel);

    void delete(TestModel testModel);

    List<TestModel> findAll();

    Optional<TestModel> findById(Long id);
}
