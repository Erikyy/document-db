package ee.erik.documentdb.integration.list;

import ee.erik.documentdb.dao.ListGenericDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.PreDestroy;
import java.util.List;
import java.util.Optional;

@Repository
public class TestRepositoryImpl implements TestRepository {

    private ListGenericDao<TestModel, Long> dao;

    @Autowired
    public void setDao(ListGenericDao<TestModel, Long> dao) {
        this.dao = dao;
        dao.setObjectInfo(TestModel.class);
    }

    @Override
    public TestModel insert(TestModel testModel) {
        return this.dao.insert(testModel);
    }

    @Override
    public TestModel update(TestModel testModel) {
        return this.dao.update(testModel);
    }

    @Override
    public void delete(TestModel testModel) {
        this.dao.delete(testModel);
    }

    @Override
    public List<TestModel> findAll() {
        return this.dao.findAll();
    }

    @Override
    public Optional<TestModel> findById(Long id) {
        return this.dao.findById(id);
    }


    @PreDestroy
    public void destroyData() {
        this.dao.destroy();
    }


}
