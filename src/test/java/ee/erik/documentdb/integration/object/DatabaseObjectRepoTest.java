package ee.erik.documentdb.integration.object;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class DatabaseObjectRepoTest {

    @Autowired
    private TestSingletonRepository repository;

    @Test
    public void shouldAddNewObject() {
        TestSingletonModel testSingletonModel = new TestSingletonModel("Test", "Test", List.of(1, 2, 3));

        TestSingletonModel saved = this.repository.save(testSingletonModel);

        assertThat(saved).isNotNull();
        assertThat(saved).isEqualTo(testSingletonModel);

        TestSingletonModel testSingletonModel2 = this.repository.getObject();
        assertThat(testSingletonModel2).isNotNull();
        testSingletonModel2.setName("New Test");
        testSingletonModel2.setSomeOtherName("Some other new name");
        testSingletonModel2.setNumbers(List.of(1, 2));

        TestSingletonModel updated = this.repository.save(testSingletonModel2);

        assertThat(updated).isNotNull();
        assertThat(updated).isEqualTo(testSingletonModel2);

        this.repository.delete();

        TestSingletonModel testSingletonModel3 = this.repository.getObject();

        assertThat(testSingletonModel3).isNull();
    }

    @Test
    public void shouldUpdateObject() {


    }

    @Test
    public void shouldDeleteObject() {

    }
}
