package ee.erik.documentdb.integration.object;

import ee.erik.documentdb.core.annotations.DatabaseObjectAsSingleton;
import ee.erik.documentdb.core.annotations.ObjectProperty;
import ee.erik.documentdb.core.annotations.OverrideDataFileName;
import ee.erik.documentdb.core.annotations.OverrideMetadataFileName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@DatabaseObjectAsSingleton(name = "custom_test_model")
@OverrideMetadataFileName(name = "custom_metadata")
@OverrideDataFileName(name = "custom_data")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TestSingletonModel {
    //there is no reason to add id field here as this is just singleton object in database file
    private String name;

    @ObjectProperty(name = "some_other_name")
    private String someOtherName;

    private List<Integer> numbers;
}
