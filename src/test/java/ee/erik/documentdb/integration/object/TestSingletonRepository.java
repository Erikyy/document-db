package ee.erik.documentdb.integration.object;

import ee.erik.documentdb.dao.ObjectGenericDao;

public interface TestSingletonRepository {
    TestSingletonModel save(TestSingletonModel entity);

    TestSingletonModel getObject();

    void delete();
}
