package ee.erik.documentdb.integration.object;

import ee.erik.documentdb.dao.ObjectGenericDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.PreDestroy;

@Repository
public class TestSingletonRepositoryImpl implements TestSingletonRepository {

    private ObjectGenericDao<TestSingletonModel> dao;

    @Autowired
    public void setDao(ObjectGenericDao<TestSingletonModel> dao) {
        this.dao = dao;
        this.dao.setObjectInfo(TestSingletonModel.class);
    }

    @Override
    public TestSingletonModel save(TestSingletonModel entity) {
        return this.dao.save(entity);
    }

    @Override
    public TestSingletonModel getObject() {
        return this.dao.getObject();
    }

    @Override
    public void delete() {
        this.dao.delete();
    }

    @PreDestroy
    public void destroyData() {
        this.dao.delete();
    }
}
